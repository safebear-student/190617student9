import base_test

class TestCases(base_test.BaseTest):
    def test_01_test_login_page_login(self):
        #click on login
        assert self.welcomepage.click_login(self.loginpage)
        #login to site
        assert self.loginpage.login(self.mainpage, "testuser","testing")

        # assert self.mainpage.press_say_something("Hi")
        assert self.mainpage.select_number_from_dd(3,2,5)

        assert self.mainpage.click_logout(self.welcomepage)
