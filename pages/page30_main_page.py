from time import sleep
from page_objects import PageObject, PageElement



from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class MainPage(PageObject):
    logout_link = PageElement(link_text="Logout")
    press_say_something_button = PageElement(css='btn btn-lg btn-success')
    first_add_dropdown = PageElement(name='num1')
    second_add_dropdown= PageElement(name='num2')

    result_field = PageElement(id_ = 'addResult')
    dropdowns_submit_button = PageElement(css='input.btn.btn-lg.btn-success')



    #alert = self.w.switch_to_alert
    def check_page(self):
        return "Logged In" in self.w.title

    def press_say_something(self,text):
        self.press_say_something_button.click()
        #alert.send_keys(text)
        # sleep(1)
        # alert.accept()
        # sleep(1)
        #

    def select_number_from_dd(self, value1, value2, result):
        select1=Select(self.first_add_dropdown)
        select2=Select(self.second_add_dropdown)

        select1.select_by_visible_text(str(value1))
        select2.select_by_visible_text(str(value2))

        self.dropdowns_submit_button.click()
        sleep(2)

        return str(result) in self.result_field.text

    def click_logout(self, welcomepage):
        self.logout_link.click()
        return welcomepage.check_page()
