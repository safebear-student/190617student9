from page_objects import PageObject, PageElement
import time
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class WelcomePage(PageObject):
    login_link = PageElement(link_text="Login")
    press_say_summat_button= PageElement(css='btn btn-lg btn-success')
    saysomething_field = PageElement(id_ = 'saySomething')


    def check_page(self):
        return "Welcome" in self.w.title



    def click_login(self, loginpage):
        self.login_link.click()
        return loginpage.check_page()
